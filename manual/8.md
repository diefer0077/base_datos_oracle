# 8. Actualizar registros (update)

Decimos que actualizamos un registro cuando modificamos alguno de sus valores.

Para modificar uno o varios datos de uno o varios registros utilizamos "update" (actualizar).

Sintaxis básica:

```sql
update NOMBRETABLA set CAMPO=NUEVOVALOR;
```

Utilizamos "update" junto al nombre de la tabla y "set" junto con el campo a modificar y su nuevo valor.

El cambio afectará a todos los registros.

Por ejemplo, en nuestra tabla "usuarios", queremos cambiar los valores de todas las claves, por "RealMadrid":

```sql
update usuarios set clave='RealMadrid';
```

Podemos modificar algunos registros, para ello debemos establecer condiciones de selección con "where".

Por ejemplo, queremos cambiar el valor correspondiente a la clave de nuestro usuario llamado "Federicolopez", queremos como nueva clave "Boca", necesitamos una condición "where" que afecte solamente a este registro:

```sql
update usuarios set clave='Boca' where nombre='Federicolopez';
```

Si Oracle no encuentra registros que cumplan con la condición del "where", un mensaje indica que ningún registro fue modificado.

Las condiciones no son obligatorias, pero si omitimos la cláusula "where", la actualización afectará a todos los registros.

También podemos actualizar varios campos en una sola instrucción:

```sql
update usuarios set nombre='Marceloduarte', clave='Marce' where nombre='Marcelo';
```

Para ello colocamos "update", el nombre de la tabla, "set" junto al nombre del campo y el nuevo valor y separado por coma, el otro nombre del campo con su nuevo valor.

# Ejercicios de laboratorio

Trabajamos con la tabla "usuarios".
Eliminamos la tabla:

```sql
drop table usuarios;
```

Creamos la tabla:

```sql
create table usuarios(
    nombre varchar2(20),
    clave varchar2(10)
);
```

Ingresamos algunos registros:

```sql
insert into usuarios (nombre,clave) values ('Marcelo','River');
insert into usuarios (nombre,clave) values ('Susana','chapita');
insert into usuarios (nombre,clave) values ('Carlosfuentes','Boca');
insert into usuarios (nombre,clave) values ('Federicolopez','Boca');
```

Cambiaremos los valores de todas las claves, por la cadena "RealMadrid":

```sql
update usuarios set clave='RealMadrid';
```

Un mensaje indica que se actualizaron 4 registros.

El cambio afectó a todos los registros, veámoslo:

```sql
select *from usuarios;
```

Necesitamos cambiar el valor de la clave del usuario llamado "Federicolopez" por "Boca":

```sql
update usuarios set clave='Boca' where nombre='Federicolopez';
```

Verifiquemos que la actualización se realizó:

```sql
select *from usuarios;
```

Vimos que si Oracle no encuentra registros que cumplan con la condición del "where", un mensaje indica que ningún registro se modifica:

```sql
update usuarios set clave='payaso' where nombre='JuanaJuarez';
```

Para actualizar varios campos en una sola instrucción empleamos:

```sql
update usuarios set nombre='Marceloduarte', clave='Marce' where nombre='Marcelo';
```

Verifiquemos que la actualización se realizó:

```sql
select *from usuarios;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table usuarios;

create table usuarios(
    nombre varchar2(20),
    clave varchar2(10)
);

insert into usuarios (nombre,clave) values ('Marcelo','River');
insert into usuarios (nombre,clave) values ('Susana','chapita');
insert into usuarios (nombre,clave) values ('Carlosfuentes','Boca');
insert into usuarios (nombre,clave) values ('Federicolopez','Boca');

update usuarios set clave='RealMadrid';

select *from usuarios;

update usuarios set clave='Boca' where nombre='Federicolopez';

select *from usuarios;

update usuarios set clave='payaso' where nombre='JuanaJuarez';

update usuarios set nombre='Marceloduarte', clave='Marce' where nombre='Marcelo';

select *from usuarios;
```

# Ejercicios propuestos

## Ejercicio 01

Trabaje con la tabla "agenda" que almacena los datos de sus amigos.

1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table agenda;

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```

2. Ingrese los siguientes registros:

```sql
insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');
```

3. Modifique el registro cuyo nombre sea "Juan" por "Juan Jose" (1 registro actualizado)

4. Actualice los registros cuyo número telefónico sea igual a "4545454" por "4445566" (2 registros)

5. Actualice los registros que tengan en el campo "nombre" el valor "Juan" por "Juan Jose" (ningún registro afectado porque ninguno cumple con la condición del "where")

## Ejercicio 02

Trabaje con la tabla "libros" de una librería.

1. Elimine la tabla y créela con los siguientes campos: titulo (cadena de 30 caracteres de longitud), autor (cadena de 20), editorial (cadena de 15) y precio (entero no mayor a 999.99):

```sql
 rop table libros;

create table libros (
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```

2. Ingrese los siguientes registros:

```sql
insert into libros (titulo, autor, editorial, precio) values ('El aleph','Borges','Emece',25.00);
insert into libros (titulo, autor, editorial, precio) values ('Martin Fierro','Jose Hernandez','Planeta',35.50);
insert into libros (titulo, autor, editorial, precio) values ('Aprenda PHP','Mario Molina','Emece',45.50);
insert into libros (titulo, autor, editorial, precio) values ('Cervantes y el quijote','Borges','Emece',25);
insert into libros (titulo, autor, editorial, precio) values ('Matematica estas ahi','Paenza','Siglo XXI',15);
```

3. Muestre todos los registros (5 registros)

4. Modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (1 registro)

5. Nuevamente, modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (ningún registro afectado porque ninguno cumple la condición)

6. Actualice el precio del libro de "Mario Molina" a 27 pesos (1 registro)

7. Actualice el valor del campo "editorial" por "Emece S.A.", para todos los registros cuya editorial sea igual a "Emece" (3 registros)
