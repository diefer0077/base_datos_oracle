# Curso: Database Programming with SQL


[1. Crear tablas (create table - describe - all_tables - drop table)](manual/1.md)

[2. Ingresar registros (insert into- select)](manual/2.md)

[3. Tipos de datos](manual/3.md)

[4. Recuperar algunos campos (select)](manual/4.md)

[5. Recuperar algunos registros (where)](manual/5.md)

[6. Operadores relacionales](manual/6.md)

[7. Borrar registros (delete)](manual/7.md)

[8. Actualizar registros (update)](manual/8.md)

[9. Comentarios](manual/9.md)

[10. Valores nulos (null)](manual/10.md)

[11. Operadores relacionales (is null)](manual/11.md)

[12. Clave primaria (primary key)](manual/12.md)

[13. Vaciar la tabla (truncate table)](manual/13.md)

[14. Tipos de datos alfanuméricos](manual/14.md)

## Modelo de presentacion

[repositorio](https://gitlab.com/senati7912512/base-datos)

## Libros referenciales

[Oracle 12c FORMS y REPORTS-Curso práctico de formación](https://drive.google.com/file/d/1vlQioFvdtYCoPY6jY0Tq_UTaro3wMjcm/edit)

[Sistemas Gestores de Bases de Datos](https://drive.google.com/file/d/1VYaSpy-uT1lghwK9hPjAr6U4BBsbBQ9D/edit)

[Administración básica de base de datos con Oracle 12c SQL](https://drive.google.com/file/d/1HGcdUc_SgbaovtvEP1uBV9ZTUbSq_lPJ/edit)

## Backup's

[HR Data Base](https://drive.google.com/file/d/1uOe4LY18W32i5yWcazwf1k-NNXMmTIXs/edit)

## Presentaciones del curso de Oracle Academy

### 1. Introducción

[1.1. Oracle Application Express](https://drive.google.com/file/d/1hjve3XEb0mpr6KzFhkFet4muM4upDkpR/edit)

[1.2. Tecnología de Base de Datos Relacional](https://drive.google.com/file/d/1Mwc5rv6ZZ96SYQaf4ixqeeaFjPrYlfsL/edit)

[1.3. Anatomía de una Sentencia SQL](https://drive.google.com/file/d/1cLFU8jCl1JDJ1h4GRJIFaIXjh4jciJYt/edit)

[1.4. Anatomía de una Sentencia SQL](https://drive.google.com/file/d/1cLFU8jCl1JDJ1h4GRJIFaIXjh4jciJYt/edit)

## Presentaciones adicionales

[1. Introducción](https://docs.google.com/presentation/d/19Fq-uNNwYiuTIOtJxIlBD7a9uOQrmwv9/edit)
