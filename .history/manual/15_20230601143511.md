# 15. Tipos de datos numéricos

Ya explicamos que al crear una tabla debemos elegir la estructura adecuada, esto es, definir los campos y sus tipos más precisos, según el caso.

Los valores numéricos no se ingresan entre comillas. Se utiliza el punto como separador de decimales.

Para almacenar valores NUMERICOS Oracle dispone de dos tipos de datos:

* 1. number(t,d): para almacenar valores enteros o decimales, positivos o negativos. Su rango va de 1.0 x 10-130 hasta 9.999...(38 nueves). Definimos campos de este tipo cuando queremos almacenar valores numéricos con los cuales luego realizaremos operaciones matemáticas, por ejemplo, cantidades, precios, etc.

El parámetro "t" indica el número total de dígitos (contando los decimales) que contendrá el número como máximo (es la precisión). Su rango va de 1 a 38. El parámetro "d" indica el máximo de dígitos decimales (escala). La escala puede ir de -84 a 127. Para definir número enteros, se puede omitir el parámetro "d" o colocar un 0.

Un campo definido "number(5,2)" puede contener cualquier número entre -999.99 y 999.99.

Para especificar número enteros, podemos omitir el parámetro "d" o colocar el valor 0.
Si intentamos almacenar un valor mayor fuera del rango permitido al definirlo, tal valor no se carga, aparece un mensaje indicando tal situación y la sentencia no se ejecuta.
Por ejemplo, si definimos un campo de tipo "number(4,2)" e intentamos guardar el valor 123.45, aparece un mensaje indicando que el valor es demasiado grande para la columna. Si ingresamos un valor con más decimales que los definidos, el valor se carga pero con la cantidad de decimales permitidos, los dígitos sobrantes se omiten.

* 2. binary_float y binary_double: almacena números flotantes con mayor precisión:

Value                           BINARY_FLOAT                    BINARY_DOUBLE
Maximum positive finite value     3.40282E+38F                    1.79769313486231E+308
Minimum positive finite value     1.17549E-38F                    2.22507485850720E-308
Para ambos tipos numéricos:

* si ingresamos un valor con más decimales que los permitidos, redondea al más cercano.

* si intentamos ingresar un valor fuera de rango, no lo acepta.

* si ingresamos una cadena, Oracle intenta convertirla a valor numérico, si dicha cadena consta solamente de dígitos, la conversión se realiza, luego verifica si está dentro del rango, si es así, la ingresa, sino, muestra un mensaje de error y no ejecuta la sentencia. Si la cadena contiene caracteres que Oracle no puede convertir a valor numérico, muestra un mensaje de error y la sentencia no se ejecuta.
  
Por ejemplo, definimos un campo de tipo "numberl(5,2)", si ingresamos la cadena '12.22', la convierte al valor numérico 12.22 y la ingresa; si intentamos ingresar la cadena '1234.56', la convierte al valor numérico 1234.56, pero como el máximo valor permitido es 999.99, muestra un mensaje indicando que está fuera de rango. Si intentamos ingresar el valor '12y.25', Oracle no puede realizar la conversión y muestra un mensaje de error.

## Ejecicios de laboratorio

Problema:
Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla:

  drop table libros;
Creamos la tabla con la siguiente estructura:

 create table libros(
  codigo number(5) not null,
  titulo varchar2(40) not null,
  autor varchar2(30),
  editorial varchar2(15),
  precio number(6,2),
  cantidad number(4)
 );
Note que definimos el campo "codigo" de tipo "number(5)", esto es porque estimamos que no tendremos más de 99999 libros, y no colocamos decimales porque necesitamos números enteros.
Como en el campo "precio" no almacenaremos valores mayores a 9999.99, definimos el campo de tipo "number(6,2)".

Como los valores para el campo "cantidad" no superarán los 9999, definimos el campo de tipo "number(4)", no colocamos decimales porque necesitamos valores enteros.

Analicemos la inserción de datos numéricos.

Intentemos ingresar un valor para "cantidad" fuera del rango definido:

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(1,'El aleph','Borges','Emece',25.60,50000);
aparece un mensaje de error y la inserción no se ejecuta.

Ingresamos un valor para "cantidad" con decimales:

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(1,'El aleph','Borges','Emece',25.60,100.2);
Lo acepta, veamos qué se almacenó:

 select *from libros;
Truncó el valor.

Ingresamos un precio con 3 decimales:

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(2,'Don quijote','Cervantes','Emece',25.123,100);
Lo acepta, veamos qué se almacenó:

 select *from libros;
Truncó el valor.

Intentamos ingresar un código con comillas (una cadena):

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(4,'Uno','Richard Bach','Planeta','50',100);
Oracle lo convierte a número.

Intentamos ingresar una cadena que Oracle no pueda convertir a valor numérico en el campo "precio":

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(5,'Alicia en el pais...','Lewis Carroll','Planeta','50.30',200);
Error.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:
 drop table libros;

 create table libros(
  codigo number(5) not null,
  titulo varchar2(40) not null,
  autor varchar2(30),
  editorial varchar2(15),
  precio number(6,2),
  cantidad number(4)
 );

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(1,'El aleph','Borges','Emece',25.60,50000);

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(1,'El aleph','Borges','Emece',25.60,100.2);

 select *from libros;

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(2,'Don quijote','Cervantes','Emece',25.123,100);

 select *from libros;

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(4,'Uno','Richard Bach','Planeta','50',100);

 insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
  values(5,'Alicia en el pais...','Lewis Carroll','Planeta','50.30',200);

## Ejercicios propuestos

Primer problema:
Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".
La tabla contiene estos datos:

 Número de Cuenta        Documento       Nombre          Saldo
 ______________________________________________________________
 1234                         25666777        Pedro Perez     500000.60
 2234                         27888999        Juan Lopez      -250000
 3344                         27888999        Juan Lopez      4000.50
 3346                         32111222        Susana Molina   1000
1- Elimine la tabla "cuentas":

  drop table cuentas;
2- Cree la tabla eligiendo el tipo de dato adecuado para almacenar los datos descriptos arriba:

* Número de cuenta: entero hasta 9999, no nulo, no puede haber valores repetidos, clave primaria;

* Documento del propietario de la cuenta: cadena de caracteres de 8 de longitud (siempre 8), no nulo;

* Nombre del propietario de la cuenta: cadena de caracteres de 30 de longitud,

* Saldo de la cuenta: valores que no superan 999999.99

 create table cuentas(
  numero number(4) not null,
  documento char(8),
  nombre varchar2(30),
  saldo number(8,2),
  primary key (numero)
 );
3- Ingrese los siguientes registros:

 insert into cuentas(numero,documento,nombre,saldo)
  values('1234','25666777','Pedro Perez',500000.60);
 insert into cuentas(numero,documento,nombre,saldo)
  values('2234','27888999','Juan Lopez',-250000);
 insert into cuentas(numero,documento,nombre,saldo)
  values('3344','27888999','Juan Lopez',4000.50);
 insert into cuentas(numero,documento,nombre,saldo)
  values('3346','32111222','Susana Molina',1000);
Note que hay dos cuentas, con distinto número de cuenta, de la misma persona.

4- Seleccione todos los registros cuyo saldo sea mayor a "4000" (2 registros)

5- Muestre el número de cuenta y saldo de todas las cuentas cuyo propietario sea "Juan Lopez" (2 registros)

6- Muestre las cuentas con saldo negativo (1 registro)

7- Muestre todas las cuentas cuyo número es igual o mayor a "3000" (2 registros)

Segundo problema:
Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.

1- Elimine la tabla:

  drop table empleados;
2- Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

 create table empleados(
  nombre varchar2(30),
  documento char(8),
  sexo char(1),
  domicilio varchar2(30),
  sueldobasico number(7,2),--máximo estimado 99999.99
  cantidadhijos number(2)--no superará los 99
 );
3- Ingrese algunos registros:

 insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos)
  values ('Juan Perez','22333444','m','Sarmiento 123',500,2);
 insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos)
  values ('Ana Acosta','24555666','f','Colon 134',850,0);
 insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos)
  values ('Bartolome Barrios','27888999','m','Urquiza 479',10000.80,4);
4- Ingrese un valor de "sueldobasico" con más decimales que los definidos (redondea los decimales al valor más cercano 800.89)

5- Intente ingresar un sueldo que supere los 7 dígitos (no lo permite)

6- Muestre todos los empleados cuyo sueldo no supere los 900 pesos

7- Seleccione los nombres de los empleados que tengan hijos (3 registros)
