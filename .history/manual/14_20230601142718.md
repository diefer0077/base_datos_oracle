# 14. Tipos de datos alfanuméricos

Ya explicamos que al crear una tabla debemos elegir la estructura adecuada, esto es, definir los campos y sus tipos más precisos, según el caso.

Para almacenar valores alfanuméricos (texto) usamos cadenas de caracteres.

Las cadenas se colocan entre comillas simples.

Podemos almacenar letras, símbolos y dígitos con los que no se realizan operaciones matemáticas, por ejemplo, códigos de identificación, números de documentos, números telefónicos. Tenemos los siguientes tipos:

* 1. char(x): define una cadena de caracteres de longitud fija determinada por el argumento "x". Si se omite el argumento, por defecto coloca 1. "char" viene de character, que significa caracter en inglés. Su rango es de 1 a 2000 caracteres.

Que sea una cadena de longitud fija significa que, si definimos un campo como "char(10)" y almacenamos el valor "hola" (4 caracteres), Oracle rellenará las 6 posiciones restantes con espacios, es decir, ocupará las 10 posiciones; por lo tanto, si la longitud es invariable, es conveniente utilizar el tipo char; caso contrario, el tipo varchar2.
Si almacenamos "hola" en un campo definido "char(10)" Oracle almacenará "hola ".

* 2. varchar2(x): almacena cadenas de caracteres de longitud variable determinada por el argumento "x" (obligatorio). Que sea una cadena de longitud variable significa que, si definimos un campo como "varchar2(10)" y almacenamos el valor "hola" (4 caracteres), Oracle solamente ocupa las 4 posiciones (4 bytes y no 10 como en el caso de "char"); por lo tanto, si la longitud es variable, es conveniente utilizar este tipo de dato y no "char", así ocupamos menos espacio de almacenamiento en disco. Su rango es de 1 a 4000 caracteres.

* 3. nchar(x): es similar a "char" excepto que permite almacenar caracteres ASCII, EBCDIC y Unicode; su rango va de 1 a 1000 caracteres porque se emplean 2 bytes por cada caracter.

* 4. nvarchar2(x): es similar a "varchar2", excepto que permite almacenar caracteres Unicode; su rango va de 1 a 2000 caracteres porque se emplean 2 bytes por cada caracter.

* 5. y 6. varchar(x) y char2(x): disponibles en Oracle8.

* 7. long: guarda caracteres de longitud variable; puede contener hasta 2000000000 caracteres (2 Gb). No admite argumento para especificar su longitud. En Oracle8 y siguientes versiones conviene emplear "clob" y "nlob" para almacenar grandes cantidades de datos alfanuméricos.

* 8. clob (Character Large OBject) y nclob: puede almacenar hasta 128 terabytes de datos de caracteres en la base de datos

* 9. blob (Binary Large OBject): puede almacenar hasta 128 terabytes de datos de binarios (imágenes, video clips, sonidos etc.)

Si intentamos almacenar en un campo alfanumérico una cadena de caracteres de mayor longitud que la definida, aparece un mensaje indicando que el valor es demasiado grande y la sentencia no se ejecuta.

Por ejemplo, si definimos un campo de tipo varchar2(10) y le asignamos la cadena 'Aprenda PHP' (11 caracteres), aparece un mensaje y la sentencia no se ejecuta.

Si ingresamos un valor numérico (omitiendo las comillas), lo convierte a cadena y lo ingresa como tal.

Por ejemplo, si en un campo definido como varchar2(5) ingresamos el valor 12345, lo toma como si hubiésemos tipeado '12345', igualmente, si ingresamos el valor 23.56, lo convierte a '23.56'. Si el valor numérico, al ser convertido a cadena supera la longitud definida, aparece un mensaje de error y la sentencia no se ejecuta.

Es importante elegir el tipo de dato adecuado según el caso.

Para almacenar cadenas que varían en su longitud, es decir, no todos los registros tendrán la misma longitud en un campo determinado, se emplea "varchar2" en lugar de "char".

Por ejemplo, en campos que guardamos nombres y apellidos, no todos los nombres y apellidos tienen la misma longitud.

Para almacenar cadenas que no varían en su longitud, es decir, todos los registros tendrán la misma longitud en un campo determinado, se emplea "char".

Por ejemplo, definimos un campo "codigo" que constará de 5 caracteres, todos los registros tendrán un código de 5 caracteres, ni más ni menos.

Para almacenar valores superiores a 4000 caracteres se debe emplear "long".

## Ejercicios de laboratorio

Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.

Eliminamos la tabla "visitantes":

```sql
drop table visitantes;
```

Creamos con la siguiente estructura:

```sql
 create table visitantes(
  nombre varchar2(30),
  edad number(2),
  sexo char(1),
  domicilio varchar2(30),
  ciudad varchar2(20),
  telefono varchar2(11)
 );
```

Los campos "nombre", "domicilio" y "ciudad" almacenarán valores cuya longitud varía, por ello elegimos el tipo "varchar2" y le damos a cada uno una longitud máxima estimando su tamaño. El campo "sexo" se define de tipo "char", porque necesitamos solamente 1 caracter "f" o "m", que siempre será fijo. El campo "telefono" también se define como varchar2 porque no todos los números telefónicos tienen la misma longitud.

Ingresamos un registro:

```sql
 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Ana Acosta',25,'f','Avellaneda 123','Cordoba','4223344');
```

Intentamos ingresar una cadena de mayor longitud que la definida en el campo "sexo":

```sql
 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Betina Bustos',32,'fem','Bulnes 234','Cordoba','4515151');
```

Aparece un mensaje de error y la sentencia no se ejecuta.

Ingresamos el mismo registro, esta vez con un sólo caracter para el campo "sexo":

```sql
 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Betina Bustos',32,'f','Bulnes 234','Cordoba','4515151');
```

Ingresamos un número telefónico olvidando las comillas, es decir, como un valor numérico:

```sql
 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Carlos Caseres',43,'m','Colon 345','Cordoba',03514555666);
```

lo convierte a cadena, veámoslo:

```sql
 select *from visitantes;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
 drop table visitantes;

 create table visitantes(
  nombre varchar2(30),
  edad number(2),
  sexo char(1),
  domicilio varchar2(30),
  ciudad varchar2(20),
  telefono varchar2(11)
 );

 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Ana Acosta',25,'f','Avellaneda 123','Cordoba','4223344');

 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Betina Bustos',32,'fem','Bulnes 234','Cordoba','4515151');

 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Betina Bustos',32,'f','Bulnes 234','Cordoba','4515151');

 insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono)
  values ('Carlos Caseres',43,'m','Colon 345','Cordoba',03514555666);

 select *from visitantes;
```

## Ejercicios propuestos

## Ejercico 01

Una concesionaria de autos vende autos usados y almacena los datos de los autos en una tabla llamada "autos".

1- Elimine la tabla "autos"

2- Cree la tabla eligiendo el tipo de dato adecuado para cada campo, estableciendo el campo "patente" como clave primaria:

```sql
 create table autos(
  patente char(6),
  marca varchar2(20),
  modelo char(4),
  precio number(8,2),
  primary key (patente)
 );
```

Hemos definido el campo "patente" de tipo "char" y no "varchar2" porque la cadena de caracteres siempre tendrá la misma longitud (6 caracteres). Lo mismo sucede con el campo "modelo", en el cual almacenaremos el año, necesitamos 4 caracteres fijos.

3- Ingrese los siguientes registros:

```sql
 insert into autos (patente,marca,modelo,precio)
  values('ABC123','Fiat 128','1970',15000);
 insert into autos (patente,marca,modelo,precio)
  values('BCD456','Renault 11','1990',40000);
 insert into autos (patente,marca,modelo,precio)
  values('CDE789','Peugeot 505','1990',80000);
 insert into autos (patente,marca,modelo,precio)
  values('DEF012','Renault Megane','1998',95000);
```

4- Ingrese un registro omitiendo las comillas en el valor de "modelo"
Oracle convierte el valor a cadena.

5- Vea cómo se almacenó.

6- Seleccione todos los autos modelo "1990"

7- Intente ingresar un registro con un valor de patente de 7 caracteres

8- Intente ingresar un registro con valor de patente repetida.

## Ejercico 02

Una empresa almacena los datos de sus clientes en una tabla llamada "clientes".

1- Elimine la tabla "clientes"

2- Créela eligiendo el tipo de dato más adecuado para cada campo:

```sql
 create table clientes(
  documento char(8) not null,
  apellido varchar2(20),
  nombre varchar2(20),
  domicilio varchar2(30),
  telefono varchar2 (11)
 );
```

3- Analice la definición de los campos. Se utiliza char(8) para el documento porque siempre constará de 8 caracteres. Para el número telefónico se usar "varchar2" y no un tipo numérico porque si bien es un número, con él no se realizarán operaciones matemáticas.

4- Ingrese algunos registros:

```sql
 insert into clientes (documento,apellido,nombre,domicilio,telefono)
  values('22333444','Perez','Juan','Sarmiento 980','4223344');
 insert into clientes (documento,apellido,nombre,domicilio,telefono)
  values('23444555','Perez','Ana','Colon 234',null);
 insert into clientes (documento,apellido,nombre,domicilio,telefono)
  values('30444555','Garcia','Luciana','Caseros 634',null);
```

5- Intente ingresar un registro con más caracteres que los permitidos para el campo "telefono"

6- Intente ingresar un registro con más caracteres que los permitidos para el campo "documento"

7- Intente ingresar un registro omitiendo las comillas en el campo "apellido"

8- Seleccione todos los clientes de apellido "Perez" (2 registros)
